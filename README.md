# brainwave-server
A simple server using the python-mindwave-mobile package developer by 
Robin Tibor Schirrmeister
(https://github.com/robintibor/python-mindwave-mobile)
## Setup
1) install the python-mindwave-mobile (see the package readme, Bluepy is a requirement)

## Running the script
1) switch on bluetooth and headset
2) run server.py
3) if "waiting for connections is shown", you can start recording with the core app.

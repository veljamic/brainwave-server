import time
from mindwavemobile.MindwaveDataPointReader import MindwaveDataPointReader
from mindwavemobile.MindwaveDataPoints import RawDataPoint
from multiprocessing.connection import Listener


def serve():
    # This initiates communication with the DroneMindControl App
    address = ('localhost', 6000)
    # This initiates the communicator with MindWaveMobile headset
    mindwave_data_point_reader = MindwaveDataPointReader()
    # This starts the communication with the headset
    mindwave_data_point_reader.start()
    idle_since_time = time.time()
    # This specifies how many values to accumulate before sending them
    frequency = 512
    # Check that the connection was successfull
    if not mindwave_data_point_reader.isConnected():
        raise ValueError("Mindwave mobile headset is not connected!")
    counter = 0
    data = []
    listener = Listener(address, authkey=b'secret')
    while True:
        print("Waiting for connection...")
        conn = listener.accept()
        current_time = time.time()
        seconds_idle = current_time - idle_since_time
        print('connection accepted from: ', listener.last_accepted)
        print("flushing {} seconds of data".format(seconds_idle))
        # there is probably a maximum size of the hidden buffer
        # the loading takes too much time this way
        # 30s seems like a reasonable upper bound
        if seconds_idle > 30:
            seconds_idle = 30
        for i in range(int(seconds_idle)):
            for j in range(500):
                mindwave_data_point_reader.readNextDataPoint()
            if i % 50 == 0:
                print("flushed {} seconds".format(i))
        trail = True
        sent_counter = 0
        while trail:
            data_point = mindwave_data_point_reader.readNextDataPoint()
            if data_point.__class__ is RawDataPoint:
                data.append(str(data_point))
            if len(data) >= frequency:
                print("sent_data {}".format(sent_counter))
                sent_counter += 1
                try:
                    conn.send(data)
                    # clear data that were sent
                    data = []
                except ConnectionAbortedError:
                    print("Connection interrupted...")
                    idle_since_time = time.time()
                    trail = False
                except ConnectionResetError:
                    print("Connection interrupted...")
                    idle_since_time = time.time()
                    trail = False


if __name__ == '__main__':
    serve()
from setuptools import setup

setup(
    name='brainwave-server',
    version='0.1',
    packages=['mindwavemobile'],
    url='https://gitlab.fit.cvut.cz/veljamic/brainwave-server',
    license='MIT',
    author='Michal Veljacik',
    author_email='veljacikm@gmail.com',
    description='A server for reading data from MindWave'
)
